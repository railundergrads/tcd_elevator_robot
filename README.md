# TCD Elevator Robot

<p> This repo contains launch files which allow robot to enter and exit elvator. More specifically, floors 0 to 2 of the Parson building elevator (will incorporate any floor in the future). Source code, mainly Python, is also provided. <p/>

## Setting up repository
<p> To setup package, open terminal and run the following: <p/>

    mkdir -p ~/<workspace-name>/src
    cd ~/<workspace-name>
    catkin_make
    cd src 
    git clone https://gitlab.com/railundergrads/tcd_elevator_robot.git
    cd .. 
    source devel/setup.bash 

<p>When opening new terminal, always remember to source bash file. Otherwise, set the source to run automatically using command below and sourcing bash.<p/>

    gedit ~/.bashrc

<p> In the future, refined requirements.txt will be included to allow for easy installation of required packages. However, the main ones used are PyTorch, OpenCV, rospy, EasyOCR, and RealSense. <p/>

## Launch files
<p> Elevator demo can be run with or without text detection. Firstly, in two separate terminals run: <p/>

    rosrun
    roslaunch turtlebot_bringup minimal.launch --screen

**Without Text Detection**

    roslaunch tcd_elevator_robot without_detector.launch

**With Text Detection**

    roslaunch tcd_elevator_robot with_detector.launch



